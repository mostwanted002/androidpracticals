package com.mostwanted002.applicationap2;


import java.util.Random;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tLeftUP;
    private TextView tRightUP;
    private TextView tLeftDOWN;
    private TextView tRightDOWN;
    private double location;
    private Random rr = new Random();
    private Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tLeftUP = findViewById(R.id.textLeftUP);
        tRightUP = findViewById(R.id.textRightUP);
        tLeftDOWN = findViewById(R.id.textLeftDOWN);
        tRightDOWN = findViewById(R.id.textRightDOWN);
        startButton = findViewById(R.id.buttonStart);
    }

    public void onClick(View view) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                location = rr.nextInt()% 4;
                switch ((int)location) {
                    case 0:{
                        clearAll();
                        tLeftUP.setText(R.string.toggleText);
                    }break;
                    case 1:{
                        clearAll();
                        tRightUP.setText(R.string.toggleText);
                    }break;
                    case 2:{
                        clearAll();
                        tLeftDOWN.setText(R.string.toggleText);
                    }break;
                    case 3:{
                        clearAll();
                        tRightDOWN.setText(R.string.toggleText);
                    }break;
                }
            }
        });
    }
    public void clearAll(){
        tLeftUP.setText("");
        tLeftDOWN.setText("");
        tRightUP.setText("");
        tRightDOWN.setText("");
    }
}