package com.mostwanted002.applicationap3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
public class MainActivity extends AppCompatActivity {
    public static final String NUMBER1 = "com.mostwanted002.applicationap3.NUMBER1";
    public static final String NUMBER2 = "com.mostwanted002.applicationap3.NUMBER2";
    public static float NUM1;
    public static float NUM2;
    EditText num1;
    EditText num2;
    Button calculate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        num1 = findViewById(R.id.num1);
        num2 = findViewById(R.id.num2);
        calculate = findViewById(R.id.calculate);
    }
    public void onClick(View view){
        Intent result = new Intent(this, resultActivity.class);
        NUM1 = Float.parseFloat(num1.getText().toString());
        NUM2 = Float.parseFloat(num2.getText().toString());
        result.putExtra(NUMBER1, NUM1);
        result.putExtra(NUMBER2, NUM2);
        startActivity(result);
    }
}
