package com.mostwanted002.applicationap3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Locale;

public class resultActivity extends AppCompatActivity {
    TextView sumLabel, sumResult, diffLabel, diffResult, prodLabel, prodResult, divLabel, divResult;
    public float num1, num2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        sumLabel = findViewById(R.id.sumLabel);
        sumResult = findViewById(R.id.sumResult);
        diffLabel = findViewById(R.id.diffLabel);
        diffResult = findViewById(R.id.diffResult);
        prodLabel = findViewById(R.id.prodLabel);
        prodResult = findViewById(R.id.productResult);
        divLabel = findViewById(R.id.divLabel);
        divResult = findViewById(R.id.divResult);
        Intent start = getIntent();
        num1 = start.getFloatExtra(MainActivity.NUMBER1, MainActivity.NUM1);
        num2 = start.getFloatExtra(MainActivity.NUMBER2, MainActivity.NUM2);
        calculate();
    }
    private void calculate(){
        float sum, diff, product, div;
        sum = num1 + num2;
        if(num1>num2)
            diff = num1 - num2;
        else
            diff = num2 - num1;
        product = num1*num2;
        div = 0;
        if (num2!=0)
            div = num1/num2;
        sumResult.setText(String.format(Locale.getDefault(), "%f", sum));
        diffResult.setText(String.format(Locale.getDefault(), "%f", diff));
        prodResult.setText(String.format(Locale.getDefault(), "%f", product));
        divResult.setText(String.format(Locale.getDefault(), "%f", div));
    }
}
