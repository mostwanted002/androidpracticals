package com.mostwanted002.applicationap4loginpopup;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
    }
    public void login(View view){
        if(username.getText().toString().equals("")||password.getText().toString().equals("")){
            Toast.makeText(this, "ENTER LOGIN CREDENTIALS!", Toast.LENGTH_SHORT).show();
        }
        else if(username.getText().toString().equals(getString(R.string.username)) && password.getText().toString().equals(getString(R.string.password))){
            Toast.makeText(this, "WELCOME! YOU ARE NOW LOGGED IN", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "username: "+getString(R.string.username)+"\n"+"password: "+getString(R.string.password), Toast.LENGTH_LONG).show();
        }
    }

}
